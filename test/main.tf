terraform {
  required_providers {
    oci = {
      source = "hashicorp/oci"
    }
  }
}

provider "oci" {
  tenancy_ocid     = var.rootcompartment
  user_ocid        = var.user_ocid
  private_key_path = "D:/TOOLS/keys/OCI_Keys/OpenSSH-and-API_private.pem"
  fingerprint      = "d6:84:25:c2:bd:ac:e7:0e:21:66:d1:a6:0b:de:48:f0"
  region           = var.region
}
