
resource "oci_identity_compartment" "tf-compartment" {
  # Required
  compartment_id = var.rootcompartment
  description    = "Compartment for Terraform resources."
  name           = "Harmony"
}

/* resource "oci_identity_compartment" "tf-compartment_2" {
  # Required
  compartment_id = var.compartment
  description    = "Compartment for Terraform resources."
  name           = "GDLNP"
} */

