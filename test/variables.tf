variable "region" {
  default = "eu-frankfurt-1"
}

variable "rootcompartment" {
  default = "ocid1.tenancy.oc1..aaaaaaaagozl47dkv6gpbkffed5imrm3f6rjiwamxhz5ns7m2wqwfoplhhzq"
}

variable "user_ocid" {
  default = "ocid1.user.oc1..aaaaaaaafvd6rnsmdhzvxmmwbf6ljvmwa7fahwvkr4mrnyeaydb34wsvzy5q"
}

/* variable "compartment" {
  default = "ocid1.compartment.oc1..aaaaaaaaocf4brnsrgngsrpfgrxxxhkvrgmb5wezjfoimtzr2c44riggswlq"
} */
